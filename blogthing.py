import os
import re
from datetime import datetime
from operator import attrgetter
from pathlib import Path
from typing import List, Tuple
from xml.etree.ElementTree import Element

import toml
from jinja2 import Environment, FileSystemLoader, select_autoescape
from markdown import Markdown
from pydantic.main import BaseModel
from pydriller import Commit, Repository


CLEANER_REG = re.compile(r"[^0-9a-zA-Z-]+")
DEDUP_REG = re.compile(r"\-+")


class Templates(BaseModel):
    post: str = "post.html"
    index: str = "index.html"


class Author(BaseModel):
    email: str
    name: str


class Config(BaseModel):
    branch: str = "master"
    posts_path: str = "posts"
    template_path: str = "templates"
    out_dir: str = "out"
    base_dir: str = "."
    templates: Templates
    author: Author
    blog_title: str


class Post(BaseModel):
    content: str
    title: str
    sanitised_title: str
    posted_on: datetime
    last_modified: datetime


class MarkdownParser(Markdown):
    def parse(self, source: str) -> Element:
        """Parses the markdown document into elementree XML representation.

        This logic is copied from the Markdown convert method to obtain just the
        XML representation without converting to HTML string.
        
        Args:
            source: The markdown document.

        Returns:
            The XML representation.
        """
        # Split into lines and run the line preprocessors.
        self.lines = source.split("\n")
        for prep in self.preprocessors:
            self.lines = prep.run(self.lines)

        # Parse the high-level elements.
        return self.parser.parseDocument(self.lines).getroot()

    def xml_to_html(self, root: Element) -> str:
        """Converts a XML representation of a markdown document into html.

        We have split the original logic at the point where it operates on the
        xml representation of the markdown.
        """
        # Run the tree-processors
        for treeprocessor in self.treeprocessors:
            newRoot = treeprocessor.run(root)
            if newRoot is not None:
                root = newRoot

        # Serialize _properly_.  Strip top-level tags.
        output = self.serializer(root)
        if self.stripTopLevelTags:
            try:
                start = output.index("<%s>" % self.doc_tag) + len(self.doc_tag) + 2
                end = output.rindex("</%s>" % self.doc_tag)
                output = output[start:end].strip()
            except ValueError as e:  # pragma: no cover
                if output.strip().endswith("<%s />" % self.doc_tag):
                    # We have an empty document
                    output = ""
                else:
                    # We have a serious problem
                    raise ValueError(
                        "Markdown failed to strip top-level "
                        "tags. Document=%r" % output.strip()
                    ) from e

        # Run the text post-processors
        for pp in self.postprocessors:
            output = pp.run(output)

        return output.strip()


def get_commits(base_dir: Path, file_path: str, branch: str) -> Tuple[Commit]:
    """Retrieves the commits for a file

    Args:
        base_dir (Path): The path of the git repository.
        file_path (str): Path to the file to retrieve commits for.
        branch (str): Which branch to retrieve commits for the file from.

    Returns:
        Tuple: The commits found for the file
    """
    repo = Repository(
        str(base_dir),
        filepath=file_path,
        only_in_branch=branch,
    )
    return tuple(repo.traverse_commits())


def extract_title(document: Element) -> Tuple[Element, str]:
    """Extracts the first h1 element as title from the XML

    Args:
        document: The document to process.

    Returns:
        The in place modified document and the title.
    """
    title = next((el for el in document if el.tag == "h1"), None)
    # have to check for type since Element resolves to false no matter?
    if isinstance(title, Element):
        document.remove(title)
        return document, title.text or ""
    else:
        return document, ""


def datetime_format(dt: datetime, format: str = "medium") -> str:
    """String formats a datetime object according to format.

    Args:
        dt: The datetime to convert.
        format: What representation to convert the datetime into.

    Returns:
        The datetime in string representation.
    """
    if format == "full":
        format = "%Y.%m.%d %H:%M:%S"
    elif format == "medium":
        format = "%Y.%m.%d %H:%M"
    return dt.strftime(format)


def parse_md_post(
    converter: MarkdownParser, post_path: Path, commits: Tuple[Commit]
) -> Post:
    """Parses a mardown blog post

    Args:
        converter: The markdown converter.
        post_path: Path to the markdown post.
        commits: All the commits related to the file.

    Returns:
        The parsed blog post.
    """
    posted: datetime = commits[0].committer_date
    last_modified: datetime = commits[-1].committer_date

    with post_path.open() as file:
        content = file.read()

    document = converter.parse(content)
    document, post_title = extract_title(document)

    sanitised_title = post_title.lower().replace(" ", "-")
    sanitised_title = CLEANER_REG.sub("", sanitised_title)
    sanitised_title = DEDUP_REG.sub("-", sanitised_title)
    post_html = converter.xml_to_html(document)

    return Post(
        content=post_html,
        title=post_title,
        sanitised_title=sanitised_title,
        posted_on=posted,
        last_modified=last_modified,
    )


if __name__ == "__main__":
    config_file = os.getenv("CONFIGFILE", "config.toml")
    raw_config = toml.load(config_file)
    config = Config(**raw_config)

    author = Author(name=config.author.name, email=config.author.email)

    # the root to it all
    base_dir = Path(config.base_dir).resolve()

    # where to find the blog posts
    posts_dir = Path(base_dir.joinpath(config.posts_path))

    # where to write out the converted content
    out_dir = Path(base_dir.joinpath(config.out_dir))

    converter = MarkdownParser()
    env = Environment(
        loader=FileSystemLoader(base_dir.joinpath(config.template_path)),
        autoescape=select_autoescape(),
    )
    env.filters["datetime_format"] = datetime_format

    # process the blog posts files
    posts: List[Post] = []
    for post_path in posts_dir.iterdir():
        post_relative = str(post_path).replace(str(base_dir), "").strip("/")
        if commits := get_commits(base_dir, post_relative, config.branch):
            posts.append(parse_md_post(converter, post_path, commits))
        else:
            print(f"blog post is not committed: {post_path.name}")

    posts = sorted(posts, key=attrgetter("posted_on"))

    # write out the blog
    template = env.get_template("home.html")
    home = template.render(blog_posts=posts, author=author, title=config.blog_title)

    if not out_dir.exists():
        out_dir.mkdir(parents=True)

    home_path = out_dir.joinpath("index.html")
    with home_path.open("w"):
        home_path.write_text(home)

    posts_dir = out_dir.joinpath("posts")
    if not posts_dir.exists():
        posts_dir.mkdir(parents=True)

    for file in posts_dir.iterdir():
        file.unlink(missing_ok=True)

    post_template = env.get_template("post.html")
    for post in posts:
        post_html = post_template.render(
            post=post, author=config.author, title=post.title
        )
        post_path = posts_dir.joinpath(f"{post.sanitised_title}.html")
        with post_path.open("w") as file:
            file.write(post_html)
