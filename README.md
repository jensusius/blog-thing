# Blog thing

Home grown "CMS" to convert blog posts written in markdown to a static blog.

The script will operate on a git repository with a certain structure
corresponding with the current configuration. The minimum set of folders are:

### templates_path
Declares where to finds jinja2 templates to render the blog posts and home page
with.

### posts_path
Where to find the blog posts written as markdown files to render. The markdown
files needs to be committed for the script to consider them.

### out_dir
The location to save the contents of the blog.

# How to run
Create a `config.toml` in the current directory or specify its location with
`CONFIGFILE` and execute `poetry run python blogthing.py`
